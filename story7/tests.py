from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from .views import aboutme

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class AboutMePageUnitTest(TestCase):
    def test_about_me_page_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_me_page_using_function(self):
        response = resolve('/about/')
        self.assertEqual(response.func, aboutme)

    def test_using_about_me_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'aboutme.html')

    def test_about_me_page_is_success(self):
        myname = "Shannia"
        response = Client().get('/about/')
        self.assertIn(myname, response.content.decode())

class AboutPageFunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		self.browser  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(AboutPageFunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(AboutPageFunctionalTest, self).tearDown()
