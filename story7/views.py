from django.shortcuts import render, redirect

def aboutme(request):
    return render(request, 'aboutme.html')