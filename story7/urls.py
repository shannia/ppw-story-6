from django.urls import path
from . import views

appname = "story7"

urlpatterns = [
    path('about/', views.aboutme, name='about'),
]