$( function() {
    $( "#accordion" ).accordion({
      collapsible: true
    });
} );

function change() {
    var bodi = document.getElementsByTagName('body')[0]
    console.log(bodi.style.backgroundColor)
    var profile = document.getElementsByClassName('profile')[0]
    var about = document.getElementsByClassName('about')[0]
    var dataprofile = document.getElementsByClassName('data-profile')[0]
    var datah = document.getElementsByClassName('data-hover')
    var datadiv = document.getElementsByClassName('data-acd')
    if(bodi.style.backgroundColor == "rgb(220, 220, 220)"){
        bodi.style.backgroundColor = "rgb(56, 52, 52)";
        bodi.style.color = 'white';
        profile.style.backgroundColor = 'white';
        about.style.backgroundColor = 'white';
        dataprofile.style.backgroundColor = 'black';
        for(var i = 0; i < datah.length; i++){
            datah[i].style.backgroundColor = "rgb(56, 52, 52)";
            datadiv[i].style.backgroundColor = 'black';    
        }
    } else {
        bodi.style.backgroundColor = "rgb(220, 220, 220)";
        bodi.style.color = "black";
        profile.style.backgroundColor = 'black';
        about.style.backgroundColor = 'black';
        dataprofile.style.backgroundColor = 'white';
        for(var i = 0; i < datah.length; i++){
            datah[i].style.backgroundColor = "rgb(220, 220, 220)";
            datadiv[i].style.backgroundColor = 'white';
        }
    }
}