from django.shortcuts import render

def bookpage(request):
    return render(request, 'bookpage.html')