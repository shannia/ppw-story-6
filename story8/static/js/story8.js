$.ajax({
    url: "https://www.googleapis.com/books/v1/volumes?q=heal",
    success: function(result){
        let books = $('#books');
        books.empty();
        const daftar_buku = result.items;
        for(var i = 0; i < daftar_buku.length; i++){
            books.append(
                "<tr>" +
                "<th scope='row'>"+ (i+1) + "</th>" +
                "<td class='image-data'>"+ "<img class='img-photo' src="+  daftar_buku[i].volumeInfo.imageLinks.thumbnail +">"+"</td>" +
                "<td class='data-title'>"+ "<div>"+ "<a href="+ daftar_buku[i].volumeInfo.infoLink + ">" + 
                "<h5>"+daftar_buku[i].volumeInfo.title+ "</h5>" + "</a>" + "</div>" + "</td>" +
                "<td class='data-author'>"+ daftar_buku[i].volumeInfo.authors + "</td>" +
                "<td class='data-author'>"+ daftar_buku[i].volumeInfo.publisher + 
                "<br/>" + daftar_buku[i].volumeInfo.publishedDate + "</td>" +
                "</tr>" 
                );
        }
    }
})

function search(book){
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + book,
        success: function(result){
            let books = $('#books');
            books.empty();
            const daftar_buku = result.items;
            for(var i = 0; i < daftar_buku.length; i++){
                books.append(
                    "<tr>" +
                    "<th scope='row'>"+ (i+1) + "</th>" +
                    "<td class='image-data'>"+ "<img class='img-photo' src="+  daftar_buku[i].volumeInfo.imageLinks.thumbnail +">"+"</td>" +
                    "<td class='data-title'>"+ "<div>"+ "<a href="+ daftar_buku[i].volumeInfo.infoLink + ">" + 
                    "<h5>"+daftar_buku[i].volumeInfo.title+ "</h5>" + "</a>" + "</div>" + "</td>" +
                    "<td class='data-author'>"+ daftar_buku[i].volumeInfo.authors + "</td>" +
                    "<td class='data-author'>"+ daftar_buku[i].volumeInfo.publisher + 
                    "<br/>" + daftar_buku[i].volumeInfo.publishedDate + "</td>" +
                    "</tr>" 
                    );
            }
        }
    });
}

$('#search').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        search(event.target.value);
    }
});

