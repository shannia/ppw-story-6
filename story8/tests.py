from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from .views import bookpage

class StoryEightUnitTest(TestCase):
    def test_book_page_is_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_book_page_using_function(self):
        response = resolve('/book/')
        self.assertEqual(response.func, bookpage)

    def test_using_bookpage_template(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'bookpage.html')

    def test_book_page_is_success(self):
        word = "book"
        response = Client().get('/book/')
        self.assertIn(word, response.content.decode())