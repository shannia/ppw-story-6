from django.urls import path
from . import views

app_name = "story9"

urlpatterns = [
    path('story9/', views.home, name='home'),
    path('story9/login', views.log_in, name='login'),
    path('story9/logout', views.log_out, name='logout'),
]