from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from .views import home, log_in, log_out

class Story9UnitTest(TestCase):
    def test_story9_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_login_is_exist(self):
        response = Client().get('/story9/login')
        self.assertEqual(response.status_code, 200)

    def test_url_logout_is_exist(self):
        response = self.client.get('/story9/logout', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response,'/story9/')

    def test_pake_fungsi_home(self):
        response = resolve('/story9/')
        self.assertEqual(response.func, home)
    
    def test_pake_fungsi_login(self):
        response = resolve('/story9/login')
        self.assertEqual(response.func,log_in)

    def test_pake_fungsi_logout(self):
        response = resolve('/story9/logout')
        self.assertEqual(response.func,log_out)

    def test_pake_template_halaman_login(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'halamanlogin.html')


    