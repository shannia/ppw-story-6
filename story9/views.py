from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout

def home(request):
    message_error = "Your account and / or password is incorrect, please try again."
    user = request.user
    if(user.is_authenticated):
        return redirect('story9:login')
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('story9:login')
    else:
        form = AuthenticationForm()
    return render(request, 'halamanlogin.html', {'form': form , 'error': message_error})

def log_in(request):
    return render(request, 'dashboard.html')

def log_out(request):
    logout(request)
    return redirect('story9:home')