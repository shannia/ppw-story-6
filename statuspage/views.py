from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Stat
from .forms import Stat_Form
from django.utils import timezone
import datetime

def status(request):
    display_form = Stat_Form()
    form = Stat_Form(request.POST or None)
    if (request.method == "POST" and form.is_valid()):
        server_time = datetime.datetime.now()
        desc = Stat(description=form['description'].value(), model_date=server_time)
        desc.save()
    status_list = Stat.objects.all().order_by('-model_date')
    response = {
        'list' : status_list,
        'server_time' : datetime.datetime.now(),
        'form' : display_form,
    }
    return render(request, "statuspage.html", response)

def move(request):
    return HttpResponseRedirect('/status')