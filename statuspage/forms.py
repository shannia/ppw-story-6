from django import forms
from .models import Stat

class Stat_Form(forms.Form):
    error = {
        'required' : 'This field is required'
    }
    desc_attr = {
        'name' : 'name_desc',
        'type' : 'text',
        'rows' : 8,
        'cols' : 50,
        'class' : 'form-control',
        'placeholder' : 'Write your mind..'
    }
    
    description = forms.CharField(label='',required=True, widget=forms.Textarea(attrs=desc_attr), max_length=300)

    class Meta:
        model = Stat
        field = ['description']
