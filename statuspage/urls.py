from django.urls import path
from . import views

app_name = "statuspage"

urlpatterns = [
    path('', views.move),
    path('status/', views.status, name='status'),
]