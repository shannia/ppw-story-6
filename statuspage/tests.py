from django.test import TestCase,LiveServerTestCase,Client
from django.urls import resolve
from .views import status
from .models import Stat
from .forms import Stat_Form
from django.utils import timezone
import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class StatusPageUnitTest(TestCase):
    
    def test_status_page_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_move_success(self):
        response = self.client.get('', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response,'/status/')

    def test_status_page_using_status_func(self):
        index = resolve('/status/')
        self.assertEqual(index.func, status)

    def test_create_new_model(self):
        time = datetime.datetime.now()
        new_status = Stat.objects.create(description='belajar', model_date=time)
        counting = Stat.objects.all().count()
        self.assertEqual(counting , 1)
        belajar = Stat.objects.filter(description='belajar')
        self.assertTrue(belajar)

    def test_form_validation(self):
        form = Stat_Form(data={'description':'test'})
        self.assertTrue(form.is_valid())

    def test_form_validation_for_blank(self):
        form = Stat_Form(data={'description':''})
        self.assertFalse(form.is_valid())

    def test_status_submit_post_success(self):
        testing = "Aku lagi update status"
        response = Client().post('/status/', {'description': testing})
        self.assertEqual(response.status_code, 200)
        self.assertIn(testing, response.content.decode())

    def test_status_submit_post_error(self):
        testing = "Aku lagi update status"
        response = Client().post('/status/', {'description': ''})
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(testing, response.content.decode())

class StatusPageFunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(StatusPageFunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(StatusPageFunctionalTest, self).tearDown()

	def test_status_is_post(self):
		self.browser.get(self.live_server_url)
		description = self.browser.find_element_by_id('id_description')
		submit = self.browser.find_element_by_name('name_submit')
		description.send_keys('lelah bgt gatau lg')
		submit.click()
		time.sleep(5)
