from django.db import models

class Stat(models.Model):
    description = models.TextField(max_length=300)
    model_date = models.DateTimeField(auto_now_add=True)